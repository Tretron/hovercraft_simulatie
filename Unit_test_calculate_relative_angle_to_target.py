# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 13:36:46 2021

@author: Tretron - Develop
"""
from hovercraft_lib import *

test_array = [
                [45,90],
                [45,-90],
                [45,0],
                [45,180],
                [45,-180],
                [45,-270],
                [0,180],
                [-315,90],
                [-315,-270]
              ]
answer_array = [135,-45,225,45,45,135,0,135,135]

failed = 0
success = 0
tests = 0
python_errors = 0
crash = False

for i in test_array:
    crash = False
    print('_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/')
    print('test', tests)
    try:
        tested_angle = calculate_relative_angle_to_target(i[0],i[1])
    except:
        python_errors +=1
        crash = True
        print('crashed on test',tests)
    if crash == False:

        if answer_array[tests] != tested_angle:
            print('test failed angle was :',tested_angle, 'is supposed to be:', answer_array[tests])
            failed += 1
        elif answer_array[tests] == tested_angle:
            print('test succesfull angle was : ' ,tested_angle)
            success +=1
        else:
            print( ' inconclusive angle was', tested_angle, 'result should be', answer_array[tests])
    tests+=1

print(tests, 'tests conducted.', success, 'passed', failed, 'failed', python_errors, 'crashes')
        