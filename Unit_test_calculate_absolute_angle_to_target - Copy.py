# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 13:36:46 2021

@author: Tretron - Develop
"""
from hovercraft_lib import *
# [Y,X], ^Y+ ,  >X+
test_array = [
                    [[0,0],[0,0]],
                    [[0,0],[0,1]],
                    [[0,0],[0.5,0.5]],
                    [[0,0],[1,0]],
                    [[0,0],[-0.5,0.5]],
                    [[0,0],[-1,0]],
                    [[0,0],[-0.5,-0.5]],
                    [[0,0],[0,-1]],
                    [[0,0],[0.5,-0.5]]
                    
                ]
answer_array = [0,0,45,90,135,180,225,270,315]

failed = 0
success = 0
tests = 0
python_errors = 0
crash = False

for i in test_array:
    crash = False
    print('_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/')
    print('test', tests)
    try:
        tested_angle = calculate_absolute_angle_to_target(i[0],i[1])
    except:
        python_errors +=1
        crash = True
        print('crashed on test',tests)
    if crash == False:

        if answer_array[tests] != tested_angle:
            print('test failed angle was :',tested_angle, 'is supposed to be:', answer_array[tests])
            failed += 1
        elif answer_array[tests] == tested_angle:
            print('test succesfull angle was : ' ,tested_angle)
            success +=1
        else:
            print( ' inconclusive angle was', tested_angle, 'result should be', answer_array[tests])
    tests+=1

print(tests, 'tests conducted.', success, 'passed', failed, 'failed', python_errors, 'crashes')
        