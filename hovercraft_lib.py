import numpy as np
import math

def PID_controller(Kp, Ki, Kd, delta_error, sum_int , error, step_time, upperlimit = 100, lowerlimit =-100):
    '''

    Parameters
    ----------
    Kd : TYPE
        DESCRIPTION.
    Ki : TYPE
        DESCRIPTION.
    Kd : TYPE
        DESCRIPTION.
    delta_error : TYPE
        DESCRIPTION.
    sum_int : TYPE
        DESCRIPTION.

    Returns
    -------
    u : TYPE
        DESCRIPTION.
    sum_int : TYPE
        DESCRIPTION.

    '''
    sum_int =sum_int + error * step_time

    u = Kp*error +Ki*sum_int + Kd*((delta_error)/step_time)
    if u>upperlimit:
        u=upperlimit
        sum_int=sum_int - error*step_time #prevent intergrator lock
    if u < lowerlimit:
        u=lowerlimit
        sum_int=sum_int -error*step_time #prevent intergrator lock
    return (u, sum_int)

def calculate_delta_error(current_error, previous_error):
    ''' 

    Parameters
    ----------
    current_error : TYPE
        DESCRIPTION.
    previous_error : TYPE
        DESCRIPTION.

    Returns
    -------
    delta_error : TYPE
        DESCRIPTION.

    '''
    delta_error = current_error-previous_error
    return delta_error

def calculate_error(set_point, position):
    error = set_point - position
    return error

def dv_dt (Force, mass):
    acceleration = Force/mass
    return acceleration
def dx_dt (acceleration, time):
    dx_dt = acceleration * time
    return dx_dt

def dx (speed, time):
    dx = speed*time
    return dx

def domega_dt (torque, moment_of_inertia):
    domega_dt = torque/moment_of_inertia
    return domega_dt

def dtheta_dt (domega_dt, omega, step_time):

    dtheta_dt = omega+domega_dt*step_time
    return dtheta_dt

def dtheta (dtheta_dt,step_time):
    dtheta = dtheta_dt*step_time
    return dtheta

def calculate_moment_of_inertia(width, hight,mass):
    moment_of_inertia = (mass/12)*(hight**2+width**2)
    return moment_of_inertia

def calculate_rotational_torque(motor_1, motor_2,radius):

    torque=((motor_1-motor_2)*radius)
    return(torque)

def calculate_percent_thrust(max_thrust, current_thrust):
    percentage = (current_thrust/max_thrust)*100
    return percentage

def calculate_force_compoments(force, mass, theta):
    force_vector = [0,0]
    force_vector[0]=np.sin(np.radians(theta))*dv_dt(force, mass)
    force_vector[1]=np.cos(np.radians(theta))*dv_dt(force, mass)
    return force_vector

def calculate_velocity(motor_1, motor_2, time_step, initial_velocity_xy=[0,0]):
    velocity_xy = list(initial_velocity_xy)
    for i in range(len(velocity_xy)):
        velocity_xy[i] = velocity_xy[i]+dx_dt(motor_1[i],time_step)+dx_dt(motor_2[i],time_step)
    return velocity_xy

def calculate_position(velocity_xy, initial_position, time_step):
    position_xy = list(initial_position)
    for i in range(len(position_xy)):
        position_xy[i] = position_xy[i] +dx(velocity_xy[i], time_step)
    return position_xy

def calculate_absolute_angle_to_target(target_pos,current_pos):
    print(target_pos, current_pos)
    angle = math.atan2(current_pos[0]-target_pos[0], current_pos[1]-target_pos[1])
    print(angle)
    if (angle < 0 and angle > (-0.5*np.pi)):
        print( '0')
        angle = angle+np.pi
    if (angle <= -0.5*np.pi and angle > -np.pi):
        print('1')
        angle = angle+(3/2)*np.pi
    if (angle <= -np.pi and angle > -(3/2)*np.pi):
        print('2')
        angle = angle+np.pi
    return math.degrees(angle)

def calculate_relative_angle_to_target(angle_target,angle_craft):
    relative_angle = 0
    if (angle_craft >= 0):
        relative_angle= 180- angle_craft%360 +angle_target%360
    elif (angle_craft < 0 and angle_target >=0):
        relative_angle = -(angle_craft%360 -180 -angle_target)
    elif (angle_craft <0 and angle_target < 0):
        relative_angle = 180 -angle_craft%360  +angle_target%360
    return relative_angle

def unpack_array(init_input_array):
    input_array = list(init_input_array)
    x, y = zip(*input_array)    
    return(x, y)

def calculate_differential_thrust(controll_signal, force, mass, theta):
    controlled_force = [0,0]
    controlled_force[0] = -controll_signal
    controlled_force[1] = controll_signal
    motor_1 = calculate_force_compoments(controlled_force[0], mass, theta)
    motor_2 = calculate_force_compoments(controlled_force[1], mass, theta)
    return motor_1,motor_2

    