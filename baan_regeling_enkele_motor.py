# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 14:22:05 2021

@author: User
"""

import matplotlib.pyplot as plt
from hovercraft_lib import *
import numpy as np

#Initial values for system
acceleration = 0
velocity= 0
distance= 0

#Simulation values
simulation_time = 40
simulation_steps = 1000
step_time = simulation_time / simulation_steps

#PID controller values
Kp = 2.5
Ki = 0
Kd = 10
set_point = 3
sum_int = 0

# Hovercraft limits
mass = 0.500
maximal_motor_thrust = 0.010*9.81
width_of_craft = 0.015
hight_of_craft = 0.030
thickness_of_craft = 0.001

#Initialize values need for logging
velocity_array = np.zeros(simulation_steps)
acceleration_array = np.zeros(simulation_steps)
distance_array = np.zeros(simulation_steps)
time_array = np.linspace(0, simulation_time, simulation_steps)
throttle_array = np.zeros(simulation_steps)
error_old = 0

for i in range(simulation_steps-1):
    error = calculate_error(set_point, distance_array[i])   
        
    delta_error = calculate_delta_error(error, error_old)
    u, sum_int = PID_controller(Kp, Ki, Kd, delta_error, sum_int , error, step_time, upperlimit = maximal_motor_thrust, lowerlimit =-maximal_motor_thrust)
    error_old = error
    acceleration_array[i] = dv_dt(u, mass)
    velocity_array[i+1] = velocity_array[i] + dx_dt(acceleration_array[i], step_time)
    distance_array[i+1] = distance_array[i] + dx(velocity_array[i], step_time)
    throttle_array[i] = calculate_percent_thrust(maximal_motor_thrust, u)
    
#starting to plot
plt.subplot(4,1,1)
plt.plot(time_array,throttle_array)
plt.ylabel('Throttle (%)')
plt.grid()
plt.subplot(4,1,2)
plt.plot(time_array, acceleration_array)
plt.ylabel('Acceleration (m/$s^2$)')
plt.grid()
plt.subplot(4,1,3)
plt.plot(time_array, velocity_array)
plt.ylabel('Speed (m/s)')
plt.grid()
plt.subplot(4,1,4)
plt.plot([0,simulation_time],[set_point,set_point],'r-')
plt.plot(time_array, distance_array)
plt.ylabel('Distance (m)')
plt.grid()
plt.show()




