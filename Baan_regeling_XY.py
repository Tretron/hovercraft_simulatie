# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 15:54:55 2021

@author: User
"""
import matplotlib.pyplot as plt
from hovercraft_lib import *
import numpy as np

#animate the movement?
animate = True

#Initial values for system [Y,X]
acceleration = [0,0]
velocity_T0= [0,0]
distance= [0,0]
theta = 0#hoek van de hovercraft
omega = 0 #degrees/s

target_point = [5,0]


#Simulation values
simulation_time = 10
simulation_steps = 10
step_time = simulation_time / simulation_steps


#PID controller values
PID_angle = [0.1,0,1, 0, 0] #KP,KI,KD, sum_int, old delta
angle_controll = 0
Kp = 2.5
Ki = 0
Kd = 10
sum_int = 0

# Hovercraft limits
mass = 0.500
maximal_motor_thrust = 0.010*9.81
maximal_stearing_thrust = maximal_motor_thrust*0.75
force = maximal_motor_thrust
width = 0.015
hight = 0.030
thickness = 0.001
engine_radius = 0.005
moment_of_inertia= calculate_moment_of_inertia(width, hight, mass)

# making a simple path
step = np.zeros(simulation_steps) # u = valve % open
step[25:] = 45.0       # step up pedal position
step[50:] = 90.0
step[75:] = 90.0 + 45.0

#Initialize values need for logging
velocity_array = [velocity_T0]
acceleration_array = []
distance_array = [distance]
theta_array = [theta]
omega_array = [omega]

time_array = np.linspace(0, simulation_time, simulation_steps)
throttle_array = np.zeros(simulation_steps)
error_old = 0

for i in range(simulation_steps-1):
    
    #calculate motor varriables
    motor_1 =  0.01
    motor_2 =  0.01
    torque = calculate_rotational_torque(motor_1, motor_2, engine_radius)
    
    motor_1_acceleratie_xy = calculate_force_compoments(motor_1, mass, theta_array[i])
    motor_2_acceleratie_xy = calculate_force_compoments(motor_2, mass, theta_array[i])
    #calculate angular parameters
    angular_acceleration = domega_dt(torque,moment_of_inertia)
    omega_array.append(dtheta_dt(angular_acceleration, omega_array[i], step_time))
    theta_array.append(theta_array[i]%360 +dtheta (omega_array[i],step_time))
    
    #calculate errors
    print('position hovercraft')
    print('Pos X', distance_array[i][0], 'Pos y', distance_array[i][1])
    angle_target = calculate_absolute_angle_to_target(target_point,distance_array[i])
    print('print angle' ,angle_target , 'print hovercraft' , theta_array[i])
    angle = calculate_relative_angle_to_target(angle_target,theta_array[i])
    
    #PID controller angle
    print('angle to target = ', angle)
    
    delta_theta = calculate_error(0, angle)
    delta_error_theta = calculate_delta_error(delta_theta, PID_angle[4])
    angle_controll, PID_angle[3]=PID_controller(PID_angle[0], PID_angle[1], PID_angle[2], PID_angle[4], PID_angle[3] , delta_theta, step_time, upperlimit = maximal_stearing_thrust, lowerlimit =-maximal_stearing_thrust)

    PID_angle[4] = delta_theta
    #calculate liniar parameters
    velocity_array.append( calculate_velocity(motor_1_acceleratie_xy,motor_2_acceleratie_xy, step_time, velocity_array[i]))
    distance_array.append(calculate_position(velocity_array[i], distance_array[i], step_time))
    print('positie = ', distance_array[i],)

    
    if animate:
        distance_array_plot= unpack_array(distance_array) 
        plt.clf()
        time = step_time*i
        title = 'Hovercraft position T=%1.2f (s)' %time
        plt.title(title)
        #plt.axline((target_point[1],target_point[0]),(distance_array_plot[1][i+1],distance_array_plot[0][i+1]), color='r', linestyle='--')
        plt.plot(distance_array_plot[1],distance_array_plot[0], linewidth=3)
        plt.scatter(target_point[1], target_point[0], s=20,)
        
        plt.xlabel('Position X (m)')
        plt.ylabel('Position Y (m)')
        plt.gca().set_aspect('equal', adjustable='datalim')
        plt.pause(step_time/10)   
        


#print results
if not animate:
    distance_array= unpack_array(distance_array)    
    plt.plot(distance_array[0],distance_array[1], linewidth=3)
    plt.scatter(target_point[1], target_point[0], s=20,)
    plt.xlabel('Position X (m)')
    plt.ylabel('Position Y (m)')
    plt.gca().set_aspect('equal', adjustable='datalim')
    plt.show()
'''
plt.plot(time_array,theta_array)
plt.xlabel('time (s)')
plt.ylabel('angle (degree)')
plt.show()
'''


 